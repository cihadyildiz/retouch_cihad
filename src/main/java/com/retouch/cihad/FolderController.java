package com.retouch.cihad;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class FolderController {
	
	@GetMapping(path = "/folders/{id}")
	public @ResponseBody Folder getFolderById(@PathVariable int id) 
	{
		Folder folder=new Folder();
		folder.setId(id);
		folder.setName("cihad");
		return folder;
	}
}
