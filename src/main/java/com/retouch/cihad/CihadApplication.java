package com.retouch.cihad;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CihadApplication {

	public static void main(String[] args) {
		SpringApplication.run(CihadApplication.class, args);
	}

}
